create or replace package Hospital is

count number;
function ��(n in int) RETURN int;
function �������(n in int) return int;
function ���(n in Varchar2) return int;

procedure �������;
procedure ����(x in number);
END Hospital;

create or replace package body Hospital is

function ��(n in int)
RETURN int
AS
non exception;
c int;
i int;
k int;
f int;
BEGIN
select Count(docs.id) into c from docs where docs.id = n;
if c=0 then 
raise non;
end if;
select docs.price into f from docs
where docs.id = n;
select Count(record.id_d) into k from record
where record.id_d = n and to_char(record.day, 'month')=to_char(sysdate, 'month');
i:=k*f;
RETURN i;
EXCEPTION
 WHEN non THEN
  raise_application_error(-20001,'����� ������ ���');
END;

function ���(n in Varchar2)
RETURN int
AS
non exception;
i int;
k int;
f int;
BEGIN
select docs.id into i from docs
where docs.price = (select max(docs.price) from docs 
                    group by docs.species 
                    having docs.species = n)
;
RETURN i;
EXCEPTION
 WHEN non THEN
  dbms_output.put_line('����� ������ ���');
END;

function �������(n in int)
RETURN int
AS
non exception;
i int;
k int;
f int;
BEGIN
select Count(patients.phone) into f from patients where patients.phone=n;
if f=0 then
raise_application_error(-20000,'��������� � ����� ������� ���');
end if;
select Count(record.check_p) into i from record
where record.id_p = (select patients.id from patients where patients.phone = n) and record.check_p=0;
RETURN i;  
END;

procedure ����(x in number)
as
osh EXCEPTION;
c int;
cursor get_inf is
select docs.species, docs.exp from docs
where docs.exp>x
order by docs.exp DESC;
begin
select Max(docs.exp) into c from docs;
if x > c
then
RAISE osh;
else
for s in get_inf
loop
dbms_output.put_line(s.species||' '||s.exp);
end loop;
END IF;
EXCEPTION
 WHEN osh THEN
  dbms_output.put_line('����� ������ ���');
end;

procedure �������
as
cursor get_inf is
select docs.species, docs.price from docs
order by docs.price DESC;
begin
for s in get_inf
loop
dbms_output.put_line(s.species||' '||s.price);
end loop;
end;
end;